package br.com.ciandt.dojoTurma2.service;

import br.com.ciandt.dojoTurma2.entity.Contato;

import java.util.List;
import java.util.Optional;

public interface IContatoService {

    List<Contato> findAll();

    List<Contato> findByNome(String nome);

    List<Contato> findBySistema(String sistema);

    Optional<Contato> findById(Long id) ;

    Contato saveOrUpdate(Contato contato);

    boolean deleteById(Long id);
}
