package br.com.ciandt.dojoTurma2.service;

import br.com.ciandt.dojoTurma2.dao.ContatoRepository;
import br.com.ciandt.dojoTurma2.entity.Contato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService implements IContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    @Override
    public List<Contato> findByNome(String nome) {
        return contatoRepository.findByNomeContainingIgnoreCase(nome);
    }

    @Override
    public List<Contato> findBySistema(String sistema) {
        return contatoRepository.findBySistema(sistema);
    }

    @Override
    public List<Contato> findAll() {
        return contatoRepository.findAll();
    }
    @Override
    public Optional<Contato> findById(Long id) {
        return contatoRepository.findById(id);
    }
    public Contato saveOrUpdate(Contato addContato) {
        return contatoRepository.save(addContato);
    }
    public boolean deleteById(Long id){
        contatoRepository.deleteById(id);
        return !contatoRepository.existsById(id);
    }

}
