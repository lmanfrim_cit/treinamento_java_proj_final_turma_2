package br.com.ciandt.dojoTurma2.dao;

import br.com.ciandt.dojoTurma2.entity.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ContatoRepository extends JpaRepository<Contato, Long> {
    List<Contato> findByNomeContainingIgnoreCase(String nome);
    List<Contato> findBySistema(String sistema);
    //Optional<Contato> findById(Long id);
}

