package br.com.ciandt.dojoTurma2.controller;

import br.com.ciandt.dojoTurma2.dao.ContatoRepository;
import br.com.ciandt.dojoTurma2.entity.Contato;
import br.com.ciandt.dojoTurma2.entity.Telefone;
import br.com.ciandt.dojoTurma2.service.ContatoService;
import br.com.ciandt.dojoTurma2.service.IContatoService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    //@Qualifier(value = "contatoService")
    ContatoService contatoService;

    @GetMapping
    public ResponseEntity<List<Contato>> contatos() {
        return new ResponseEntity<>(
                contatoService.findAll(), HttpStatus.BAD_GATEWAY);
    }

    /*public List<Contato> contatos() {
        return contatoService.findAll();
    }*/

    @GetMapping("/nome/{nome}")
    public List<Contato> contatoByNome(@PathVariable String nome) {
        return contatoService.findByNome(nome);
    }

    @GetMapping("/sistema/{nome}")
    public List<Contato> contatoBySistema(@PathVariable String nome) {
        return contatoService.findBySistema(nome);
    }

    @GetMapping("/{id}")
    //@ResponseStatus()
    public Optional<Contato> contatoById(@PathVariable Long id) {

        List<Telefone> telefones = restTemplate.getForObject("http://telefone-service/telefone/contato/" + id, List.class);

        Optional<Contato> contato = contatoService.findById(id);
        contato.get().setTelefones(telefones);

        return contato;
    }

    @PostMapping
    public Contato add(@RequestBody Contato contato) {
        return contatoService.saveOrUpdate(contato);
    }

    @DeleteMapping("{id}")
    public boolean deleteById(@PathVariable Long id) {
        return contatoService.deleteById(id);
    }
}
